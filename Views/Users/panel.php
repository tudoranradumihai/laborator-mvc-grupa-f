<h1>Admin Panel</h1>

<h3>Categories</h3>
<a href="<?php URL::show("Categories","new"); ?>">Add Category</a> |
<a href="<?php URL::show("Categories","list"); ?>">Show All Categories</a>
<br />
<h3>Products</h3>
<a href="<?php URL::show("Products","new"); ?>">Add Product</a> |
<a href="<?php URL::show("Products","list"); ?>">Show All Products</a>
<br />