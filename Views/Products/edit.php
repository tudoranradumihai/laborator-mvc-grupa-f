<h1>Edit Product <small>- <?php echo $product->name ?></small></h1>
<form method="POST" action="<?php URL::show("Products","update"); ?>">
	<input type="hidden" name="id" value="<?php echo $product->id ?>" />
	<div class="form-group">
		<label for="">Name</label>
		<input type="text" name="name" class="form-control" id="" placeholder="..." value="<?php echo $product->name ?>">
	</div>
	<div class="form-group">
    	<label for="">Example textarea</label>
    	<textarea class="form-control" name="description" id="" rows="3"><?php echo $product->description ?></textarea>
 	</div>
	<div class="form-group">
		<label for="">Stock</label>
		<input type="text" name="stock" class="form-control" id="" placeholder="50" value="<?php echo $product->stock ?>">
	</div>
	<div class="form-group">
		<label for="">Price</label>
		<input type="text" name="price" class="form-control" id="" placeholder="100.00" value="<?php echo $product->price ?>">
	</div>
	<div class="form-group">
		<label for="">Promotional Price</label>
		<input type="text" name="promotional" class="form-control" id="" placeholder="100.00" value="<?php echo $product->promotional ?>">
	</div>
	<div class="form-group">
		<label for="">Category</label>
		<select class="form-control" name="category">
			<option value="">...</option>
			<?php foreach($categories as $category) { ?>
				<option value="<?php echo $category->id ?>" <?php if($product->category==$category->id) { echo "selected"; } ?>><?php echo $category->name ?></option>
			<?php } ?>
		</select>
	</div>
	<button type="submit" class="btn btn-primary mb-2">Edit Product</button>
</form>