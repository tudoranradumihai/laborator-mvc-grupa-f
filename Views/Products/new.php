<h1>Add New Product</h1>
<form method="POST" action="<?php URL::show("Products","create"); ?>">
	<div class="form-group">
		<label for="">Name</label>
		<input type="text" name="name" class="form-control" id="name" placeholder="...">
	</div>
	<div class="form-group">
		<label for="">Route</label>
		<input type="text" name="route" class="form-control" id="routes" placeholder="...">
	</div>
	<div class="form-group">
    	<label for="">Example textarea</label>
    	<textarea class="form-control" name="description" id="" rows="3"></textarea>
 	</div>
	<div class="form-group">
		<label for="">Stock</label>
		<input type="text" name="stock" class="form-control" id="" placeholder="50">
	</div>
	<div class="form-group">
		<label for="">Price</label>
		<input type="text" name="price" class="form-control" id="" placeholder="100.00">
	</div>
	<div class="form-group">
		<label for="">Category</label>
		<select class="form-control" name="category">
			<option value="">...</option>
			<?php foreach($categories as $category) { ?>
				<option value="<?php echo $category->getId() ?>"><?php echo $category->getName() ?></option>
			<?php } ?>
		</select>
	</div>
	<button type="submit" class="btn btn-primary mb-2">Create Product</button>
</form>