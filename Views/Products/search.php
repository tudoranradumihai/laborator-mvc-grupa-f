<form class="form-inline" method="GET" action="<?php URL::show("Products","search");?>">
	<input type="hidden" name="C" value="Products" />
<input type="hidden" name="A" value="search" />
<div class="row">
<div class="col-md-2">
<b>Categorii</b><br>

<?php foreach($categories as $category) { ?>
<a href="<?php URL::show("Categories","show",$category->getId()) ?>"><?php echo $category->getName() ?></a><br />
<?php } ?>
<br>

<b>Pret</b><br>
<?php if (count($priceFilter)>0){ ?>

<?php for ($i=0;$i<count($priceFilter);$i++) { ?>

<?php
if($i==0){
	if(array_key_exists("price", $_GET)){
		if($_GET["price"]=="0-".$priceFilter[$i]){
			$temporary = "checked";
		} else {
			$temporary = "";
		}
	} else {
			$temporary = "";
		}
	echo '<input type="radio" name="price" value="0-'.$priceFilter[$i].'" '.$temporary.' /> ';
	echo "Sub ".$priceFilter[$i]."<br />";
	if(array_key_exists("price", $_GET)){
		if($_GET["price"]==$priceFilter[$i]."-".$priceFilter[$i+1]){
			$temporary = "checked";
		} else {
			$temporary = "";
		}
	} else {
			$temporary = "";
		}
	echo '<input type="radio" name="price" value="'.$priceFilter[$i].'-'.$priceFilter[$i+1].'" '.$temporary.' /> ';
	echo $priceFilter[$i]." - ".$priceFilter[$i+1];
} elseif($i==count($priceFilter)-1){
	if(array_key_exists("price", $_GET)){
		if($_GET["price"]==$priceFilter[$i]."-".$maxProductPrice){
			$temporary = "checked";
		} else {
			$temporary = "";
		}
	} else {
			$temporary = "";
		}
	echo '<input type="radio" name="price" value="'.$priceFilter[$i].'-'.$maxProductPrice.'" '.$temporary.' /> ';
	echo "Peste ".$priceFilter[$i];
} else {
	if(array_key_exists("price", $_GET)){
		if($_GET["price"]==$priceFilter[$i]."-".$priceFilter[$i+1]){
			$temporary = "checked";
		} else {
			$temporary = "";
		}
	} else {
			$temporary = "";
		}
	echo '<input type="radio" name="price" value="'.$priceFilter[$i].'-'.$priceFilter[$i+1].'" '.$temporary.' /> ';
	echo $priceFilter[$i]." - ".$priceFilter[$i+1];
}
?>
<br>
<?php } ?>


<?php } ?>
<button type="submit" class="btn btn-primary mb-2">Filter</button>
 


</div>
<div class="col-md-10">
<div class="row">
<div class="form-group mx-sm-3 mb-2">
<label for="inputPassword2" class="sr-only">Search</label>
<input type="text" class="form-control" id="inputPassword2" placeholder="..." name="search" value="<?php echo $search ?>">
</div>
<button type="submit" class="btn btn-primary mb-2">Search</button>


</div>
<div class="row">
	<?php foreach($products as $product) { ?>
	<div class="card col-md-4" style="width: 18rem;">
		<div class="card-body">
			<h5 class="card-title"><a href="<?php URL::show("Products","show",$product->getId()); ?>"><?php echo $product->getName(); ?></a></h5>
			<p class="card-text"><?php echo $product->getDescription(); ?></p>
			<div class="text-right"><?php echo $product->getPrice(); ?> &euro;</div>
			<!--
			<a href="#" class="btn btn-primary">Go somewhere</a>
			-->
		</div>
	</div>
	<?php } ?>
</div>
</div>
</div>
</form>
<?php
var_dump($this);