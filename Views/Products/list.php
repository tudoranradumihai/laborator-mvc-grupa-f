<div class="text-right">
	<a href="<?php URL::show("Products","new"); ?>">
		<button type="button" class="btn btn-primary">Add New Product</button>
	</a>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Price</th>
				<th>Stock</th>
				<th colspan="2">Actions</th>
			</tr>
			<?php foreach($products as $product) { ?>
			<tr>
				<td><?php echo $product->id ?></td>
				<td><?php echo $product->name ?></td>
				<td><?php echo $product->price ?></td>
				<td><?php echo $product->stock ?></td>
				<td><a href="<?php URL::show("Products","edit",$product->id) ?>">Edit</a></td>
				<td><a href="<?php URL::show("Products","delete",$product->id) ?>">Delete</a></td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>