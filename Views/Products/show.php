<div>
	<h1><a href="<?php URL::show("Products","show",$product->getId()); ?>"><?php echo $product->getName(); ?></a></h1>
	<p><?php echo $product->getDescription() ?></p>
	<h3><?php echo $product->getPrice(); ?></h3>
	<form method="POST" action="<?php URL::show("CartItems","create"); ?>">
		<input type="hidden" name="product" value="<?php echo $product->getId(); ?>">
		<input type="text" name="quantity" value="1">
		<input type="submit" value="Add To Cart">
	</form>
</div>