<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Product</th>
      <th scope="col">Quantity</th>
      
      <th scope="col">Price</th>
      <th scope="col"></th>
    </tr>
  </thead>
  	<?php 
  		$contor = 0;
  		$total = 0;
  		$array=[1,2,3,4,5,6,7,8,9,10];
  		foreach ($items as $item) {
  			$contor++;
	?>
	<tr class="item-<?php echo $item->getId() ?>">
		<td><?php echo $contor ?></td>
		<td><?php echo $item->getProduct(true)->getName(); ?></td>
		<td>
			<select class="quantity" data-id="<?php echo $item->getId() ?>">
				<?php foreach($array as $value) { ?>
				<option <?php if($value==$item->getQuantity()) {echo "selected";} ?>><?php echo $value?></option>
				<?php } ?>
			</select>
		</td>

		<td>
			<span id="price-<?php echo $item->getId() ?>"><?php
				$price = $item->getQuantity()*$item->getProduct(true)->getPrice();
				$total += $price;
				echo number_format($price,2,".","");
			?></span> &euro;
			<input id="price-hidden-<?php echo $item->getId() ?>" type="hidden" class="hidden-price" value="<?php echo $price ?>">
		</td>
		<td><a class="delete-item" data-id="<?php echo $item->getId() ?>" href="">Delete</a></td>
	</tr>
	<?php } ?>
	<tr>
		<td colspan="3"></td>
		<td><span id="total-price"><?php echo number_format($total,2,".",""); ?></span> &euro;</td>
	</tr>
</table>