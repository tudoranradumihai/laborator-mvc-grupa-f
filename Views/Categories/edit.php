<h1>Edit Category <small>- <?php echo $category->name ?></small></h1>
<form method="POST" action="<?php URL::show("Categories","update"); ?>">
	<input type="hidden" name="id" value="<?php echo $category->id ?>" />
	<div class="form-group">
		<label for="">Name</label>
		<input type="text" name="name" class="form-control" id="" placeholder="..." value="<?php echo $category->name ?>">
	</div>
	<div class="form-group">
    	<label for="">Example textarea</label>
    	<textarea class="form-control" name="description" id="" rows="3"><?php echo $category->description ?></textarea>
 	</div>
	<button type="submit" class="btn btn-primary mb-2">Edit Category</button>
</form>