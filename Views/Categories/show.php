<div>
	<h1><a href="<?php URL::show("Categories","show",$category->id); ?>"><?php echo $category->name; ?></a></h1>
	<p><?php echo $category->description ?></p>
</div>

<div class="row">
	<?php foreach($products as $product) { ?>
	<div class="card col-md-4" style="width: 18rem;">
		<div class="card-body">
			<h5 class="card-title"><a href="<?php URL::show("Products","show",$product->id); ?>"><?php echo $product->name; ?></a></h5>
			<p class="card-text"><?php echo $product->description; ?></p>
			<div class="text-right"><?php echo $product->price; ?> &euro;</div>
			<!--
			<a href="#" class="btn btn-primary">Go somewhere</a>
			-->
		</div>
	</div>
	<?php } ?>
</div>