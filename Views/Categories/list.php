<div class="text-right">
	<a href="<?php URL::show("Categories","new"); ?>">
		<button type="button" class="btn btn-primary">Add New Category</button>
	</a>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="table">
			<tr>
				<th>#</th>
				<th>Name</th>
				<th colspan="2">Actions</th>
			</tr>
			<?php foreach($categories as $category) { ?>
			<tr>
				<td><?php echo $category->id ?></td>
				<td><?php echo $category->name ?></td>
				<td><a href="<?php URL::show("Categories","edit",$category->id) ?>">Edit</a></td>
				<td><a href="<?php URL::show("Categories","delete",$category->id) ?>">Delete</a></td>
			</tr>
			<?php } ?>
		</table>
	</div>
</div>