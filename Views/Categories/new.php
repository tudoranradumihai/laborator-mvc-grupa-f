<h1>Add New Category</h1>
<form method="POST" action="<?php URL::show("Categories","create"); ?>">
	<div class="form-group">
		<label for="">Name</label>
		<input type="text" name="name" class="form-control" id="" placeholder="...">
	</div>
	<div class="form-group">
    	<label for="">Description</label>
    	<textarea class="form-control" name="description" id="" rows="3"></textarea>
 	</div>
	<button type="submit" class="btn btn-primary mb-2">Create Category</button>
</form>