<?php

Class CartItemsController extends Controller {

	private $cartRepository;
	private $cartItemsRepository;

	public function __construct(){
		$this->cartRepository = new CartRepository();
		$this->cartItemsRepository = new CartItemsRepository();
	}

	public function defaultAction(){

	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$item = $this->cartItemsRepository->findByCartAndProduct($_SESSION["cart"],$_POST["product"]);
			if($item){
				$item->setQuantity(intval($_POST["quantity"])+intval($item->getQuantity()));
				$this->cartItemsRepository->update($item);
			} else {
				$item = new CartItems();
				$item->setCart($_SESSION["cart"]);
				$item->setProduct($_POST["product"]);
				$item->setQuantity($_POST["quantity"]);
				$this->cartItemsRepository->insert($item);
			}
		}
		URL::redirect("Cart","default");
	}

	public function updateAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$item = $this->cartItemsRepository->findById($_POST["id"]);
			if($_POST["quantity"]=="0"){
				$this->cartItemsRepository->delete($item->getId());
				echo "TRUE";
			} else {
				$item->setQuantity($_POST["quantity"]);
				$this->cartItemsRepository->update($item);
				echo $item->getProduct(true)->getPrice()*$item->getQuantity();
			}
		}
	}
}