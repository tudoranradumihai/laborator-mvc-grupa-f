<?php

Class CategoriesController extends Controller {

	private $categoriesRepository;

	public function __construct(){
		$this->categoriesRepository = new CategoriesRepository();
	}

	public function defaultAction(){
		// ...
	}

	public function listAction(){
		if(array_key_exists("admin", $_COOKIE)){
			$categories = $this->categoriesRepository->findAll();
			require "Views/Categories/list.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function showAction(){
		if(array_key_exists("ID", $_GET)){
			$category = $this->categoriesRepository->findByID($_GET["ID"]);
			$productsRepository = new ProductsRepository();
			$products = $productsRepository->findByCategory($_GET["ID"]);
			require "Views/Categories/show.php";
		} else {
			URL::redirect("Categories","list");
		}
	}

	public function searchAction(){
		if(array_key_exists("search", $_GET)){
			$search = $_GET["search"];
			$categories = $this->categoriesRepository->findByFilter($search);
		} else {
			$search = "";
			$categories = $this->categoriesRepository->findAll();
		}
		require "Views/Categories/search.php";
	}

	public function newAction(){
		if(array_key_exists("admin", $_COOKIE)){
			require "Views/Categories/new.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$category = new Categories();
			foreach($category as $property => $value){
				if(array_key_exists($property, $_POST)){
					$category->$property = $_POST[$property];
				}
			}
			$this->categoriesRepository->insert($category);
		}
		URL::redirect("Categories","list");
	}

	public function editAction(){
		if(array_key_exists("admin", $_COOKIE)){
			$category = $this->categoriesRepository->findByID($_GET["ID"]);
			require "Views/Categories/edit.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function updateAction(){
		if(array_key_exists("admin", $_COOKIE)){
			$category = new Categories();
			foreach($category as $property => $value){
				if(array_key_exists($property, $_POST)){
					$category->$property = $_POST[$property];
				}
			}
			$this->categoriesRepository->update($category);
			URL::redirect("Categories","list");
		} else {
			URL::redirect("Users","login");
		}
	}

	public function deleteAction(){
		$this->categoriesRepository->delete($_GET["ID"]);
		URL::redirect("Categories","list");
	}


}