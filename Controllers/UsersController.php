<?php

Class UsersController extends Controller {

	private $usersRepository;

	public function __construct(){
		$this->usersRepository = new UsersRepository();
	}

	public function defaultAction(){

	}

	public function newAction(){
		require "Views/Users/new.php";
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$user = new Users();
			$user->firstname	= $_POST["firstname"];
			$user->lastname		= $_POST["lastname"];
			$user->email 		= $_POST["email"];
			$user->password 	= md5($_POST["password"]);
			if(array_key_exists("image", $_FILES)){
				if($_FILES["image"]["error"]==0 && $_FILES["image"]["size"]>0){
					if(!file_exists("Resources/Uploads")){
						mkdir("Resources/Uploads");
					}
					if(!file_exists("Resources/Uploads/Users")){
						mkdir("Resources/Uploads/Users");
					}
					$extension = explode(".",$_FILES["image"]["name"]);
					$filename = date("YmdHis")."-".rand(10000,99999).".".$extension[count($extension)-1];
					$path = "Resources/Uploads/Users/".$filename;
					if(move_uploaded_file($_FILES["image"]["tmp_name"], $path)){
						$user->image = $filename;
					}
				}
			}
			$result = $this->usersRepository->insert($user);
			require "Views/Users/create.php";
		}
	}

	public function loginAction(){
		if(array_key_exists("email", $_SESSION)){
			$email = $_SESSION["email"];
			unset($_SESSION["email"]);
		} else {
			$email = "";
		}
		if(array_key_exists("error", $_SESSION)){
			$error = $_SESSION["error"];
			unset($_SESSION["error"]);
		} else {
			$error = "";
		}
		require "Views/Users/login.php";
	}

	public function connectAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$usersRepository = new UsersRepository();
			$user = $this->usersRepository->checkCredentials($_POST["email"],md5($_POST["password"]));
			if(!is_null($user)){
				$_SESSION["user"] = serialize($user);


				$configuration = require "Configurations/Configuration.php";
				$domain = str_replace("http://","",$configuration["GENERAL"]["DOMAIN"]);
				if(substr($domain,-1)=="/"){
					$domain = substr($domain,0,-1);
				}
   				setcookie("user",$user->getId(),time()+3600,"/",$domain);
   				if($user->getAdministrator()){
   					setcookie("admin",$user->getId(),time()+3600,"/",$domain);
   					URL::redirect("Users","panel");
   				} else {
   					URL::redirect("Users","profile");
   				}
			} else {
				$status = $this->usersRepository->checkEmail($_POST["email"]);
				if($status){
					$_SESSION["email"] = $_POST["email"];
					$_SESSION["error"] = "The password is incorrect";
				} else {
					$_SESSION["error"] = "The email and password are incorrect.";
				}
				URL::redirect("Users","login");
			}
		} else {
			URL::redirect("Users","login");
		}
	}

	public function disconnectAction(){
		$configuration = require "Configurations/Configuration.php";
		$domain = str_replace("http://","",$configuration["GENERAL"]["DOMAIN"]);
		if(substr($domain,-1)=="/"){
			$domain = substr($domain,0,-1);
		}
		if(array_key_exists("user", $_COOKIE)){
			setcookie("user",NULL,time()-3600,"/",$domain);
		}
		if(array_key_exists("admin", $_COOKIE)){
			setcookie("admin",NULL,time()-3600,"/",$domain);
		}
		if(array_key_exists("user", $_SESSION)){
			unset($_SESSION["user"]);
		}
		URL::redirect("Users","login");
	}

	public function profileAction(){
		if(array_key_exists("user", $_COOKIE)){
			$user = unserialize($_SESSION["user"]);
			require "Views/Users/profile.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function panelAction(){
		if(array_key_exists("admin", $_COOKIE)){
			$user = unserialize($_SESSION["user"]);
			require "Views/Users/panel.php";
		} else {
			URL::redirect("Users","login");
		}
	}

}