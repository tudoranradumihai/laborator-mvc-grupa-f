<?php

Class CartController extends Controller {

	private $cartRepository;

	public function __construct(){
		parent::__construct();
		$this->cartRepository = new CartRepository();
	}

	public function defaultAction(){
		$cart = $this->cartRepository->findById($_SESSION["cart"]);
		$cartItemsRepository = new CartItemsRepository();
		$items = $cartItemsRepository->findByCart($cart->getId());
		$this->view->render(array(
			"items" => $items
		));
	}

	public function set(){
		$cart = $this->cartRepository->findByFilter("session",session_id());
		if(count($cart)>0){
			$cart = array_pop($cart);
			if(array_key_exists("user", $_COOKIE) && $cart->getUser()==""){
				$_SESSION["cart"] = $cart->getId();
				$cart->setUser($_COOKIE["user"]);
				$this->cartRepository->update($cart);
			}
		} else {
			if(array_key_exists("user", $_COOKIE)){
				$cart = $this->cartRepository->findByFilter("user",$_COOKIE["user"]);
				if(count($cart)>0){
					$cart = array_pop($cart);
					$_SESSION["cart"] = $cart->getId();
					$cart->setSession(session_id());
					$this->cartRepository->update($cart);
				} else {
					$cart = new Cart();
					$cart->setSession(session_id());
					if(array_key_exists("user", $_COOKIE)){
						$cart->setUser($_COOKIE["user"]);
					}
					$_SESSION["cart"] = $this->cartRepository->insert($cart);
				}
			} else {
				$cart = new Cart();
				$cart->setSession(session_id());
				if(array_key_exists("user", $_COOKIE)){
					$cart->setUser($_COOKIE["user"]);
				}
				$_SESSION["cart"] = $this->cartRepository->insert($cart);

			}
		}
	}

}