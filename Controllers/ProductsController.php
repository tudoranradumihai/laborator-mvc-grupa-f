<?php

Class ProductsController extends Controller {

	private $productsRepository;

	public function __construct(){
		parent::__construct();
		$this->productsRepository = new ProductsRepository();
	}

	public function defaultAction(){
		// ...
	}

	public function listAction(){
		if(array_key_exists("admin", $_COOKIE)){
			$products = $this->productsRepository->findAll();
			require "Views/Products/list.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function showAction(){
		if(array_key_exists("ID", $_GET)){
			$product = $this->productsRepository->findByID($_GET["ID"]);
			require "Views/Products/show.php";
		} else {
			URL::redirect("Products","list");
		}
	}

	public function searchAction(){
		$categoriesRepository = new CategoriesRepository();
		$categories = $categoriesRepository->findAll();
		$filters = array();
		if(array_key_exists("search", $_GET)){
			$search = $_GET["search"];	
			$filters["name"] = $search;
		} else {
			$search = "";
		}
		if(array_key_exists("price", $_GET)){
			$filters["price"] = $_GET["price"];
		}

		$products = $this->productsRepository->findByFilters($filters);
		$allProducts = $this->productsRepository->findAll();
		$priceFilter = array();
		if(count($allProducts)>0){
			foreach($allProducts as $product){
				if(isset($minProductPrice)){
					if($minProductPrice>$product->getPrice()){
						$minProductPrice = $product->getPrice();
					}
				} else {
					$minProductPrice = $product->getPrice();
				}
				if(isset($maxProductPrice)){
					if($maxProductPrice<$product->getPrice()){
						$maxProductPrice = $product->getPrice();
					}
				} else {
					$maxProductPrice = $product->getPrice();
				}
			}
			$diff = $maxProductPrice-$minProductPrice;
			if($diff>100){
				$f1 = ceil($minProductPrice/100)*100;
				$f2 = floor($maxProductPrice/100)*100;
				$f3 = (($f2-$f1)/2)+$f1;
				array_push($priceFilter,$f1);
				array_push($priceFilter,$f2);
				array_push($priceFilter,$f3);
			}
			sort($priceFilter);
		}
		$this->view->render(array(
			"categories" => $categories,
			"priceFilter" => $priceFilter,
			"maxProductPrice" => $maxProductPrice,
			"search" => $search,
			"products" => $products
		));
	}

	public function newAction(){
		if(array_key_exists("admin", $_COOKIE)){
			$categoriesRepository = new CategoriesRepository();
			$categories = $categoriesRepository->findAll();
			require "Views/Products/new.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function createAction(){
		if($_SERVER["REQUEST_METHOD"]=="POST"){
			$product = new Products();
			foreach($product->getProperties() as $property){
				if(array_key_exists($property, $_POST)){
					$product->{"set".ucfirst($property)}($_POST[$property]);
				}
			}
			$identifier = $this->productsRepository->insert($product);
			if($identifier){
				$route = new Routes();
				$route->setUrl($_POST["route"]);
				$route->setController("Products");
				$route->setAction("show");
				$route->setIdentifier($identifier);
				$routesRepository = new RoutesRepository();
				$routesRepository->insert($route);
			}
		}
		URL::redirect("Products","list");
	}

	public function editAction(){
		if(array_key_exists("admin", $_COOKIE)){
			$product = $this->productsRepository->findByID($_GET["ID"]);
			$categoriesRepository = new CategoriesRepository();
			$categories = $categoriesRepository->findAll();
			require "Views/Products/edit.php";
		} else {
			URL::redirect("Users","login");
		}
	}

	public function updateAction(){
		if(array_key_exists("admin", $_COOKIE)){
			$product = new Products();
			foreach($product as $property => $value){
				if(array_key_exists($property, $_POST)){
					$product->$property = $_POST[$property];
				}
			}
			$this->productsRepository->update($product);
			URL::redirect("Products","list");
		} else {
			URL::redirect("Users","login");
		}
	}

	public function deleteAction(){
		$this->productsRepository->delete($_GET["ID"]);
		URL::redirect("Products","list");
	}


}