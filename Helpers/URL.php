<?php

Class URL {

	public static function generate($controller,$action,$identifier = NULL){
		$configuration = require "Configurations/Configuration.php";
		$link = $configuration["GENERAL"]["DOMAIN"];
		if($configuration["GENERAL"]["REWRITE"]){
			$routesRepository = new RoutesRepository();
			$routes = $routesRepository->findByParameters($controller,$action,$identifier);
			if(count($routes)>0){
				$route = array_pop($routes);
				$link .= $route->getUrl();
			} else {
				$link .= "$controller/$action";
				if(!empty($identifier)){
					$link .= "/$identifier";
				}
			}
		} else {
			$link .= "index.php?C=$controller&A=$action";
			if(!empty($identifier)){
				$link .= "&ID=$identifier";
			}
		}
		return $link;
	}

	public static function show($controller,$action,$identifier=NULL){
		echo self::generate($controller,$action,$identifier);
	}

	public static function redirect($controller,$action,$identifier=NULL){
		header("Location: ".self::generate($controller,$action,$identifier));
	}



}



