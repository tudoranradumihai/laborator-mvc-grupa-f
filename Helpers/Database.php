<?php

Class Database {

	private $connection;

	public function __construct(){
		$configuration = require "Configurations/Configuration.php";
		$this->connection = new mysqli(
			$configuration["DATABASE"]["HOSTNAME"],
			$configuration["DATABASE"]["USERNAME"],
			$configuration["DATABASE"]["PASSWORD"],
			$configuration["DATABASE"]["DATABASE"]
		);
	}

	public function query($query){
		return $this->connection->query($query);
	}

	public function last(){
		return $this->connection->insert_id;
	}

	public function __destruct(){
		$this->connection->close();
	}



}