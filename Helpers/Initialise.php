<?php
function initialise(){
	if(array_key_exists("REQUEST", $_GET)){
		$request = trim($_GET["REQUEST"]);
		if($request[strlen($request)-1]=="/"){
			$request = substr($request,0,-1);
		}
		$routesRepository = new RoutesRepository();
		$routes = $routesRepository->findByFilter("url",$request); 
		if(count($routes)>0){
			$route = array_pop($routes);
			if($route->getController()){
				$_GET["C"] = $route->getController();
			}
			if($route->getAction()){
				$_GET["A"] = $route->getAction();
			}
			if($route->getIdentifier()){
				$_GET["ID"] = $route->getIdentifier();
			}
		} else {
			$request = explode("/",$_GET["REQUEST"]);
			if(array_key_exists(0, $request)){
				$_GET["C"] = $request[0];
			}
			if(array_key_exists(1, $request)){
				$_GET["A"] = $request[1];
			}
			if(array_key_exists(2, $request)){
				$_GET["ID"] = $request[2];
			}
		}
	}
	if(array_key_exists("C", $_GET)){
		$controller = ucfirst($_GET["C"])."Controller";
	} else {
		$controller = "DefaultController";
	}
	if(array_key_exists("A", $_GET)){
		$action = lcfirst($_GET["A"])."Action";
	} else {
		$action = "defaultAction";
	}
	if(class_exists($controller)){
		$object = new $controller();
		if(method_exists($object, $action)){
			//echo "<!-- $controller::$action -->"."\n";
			$object->$action();
		} else {
			die("Method '$controller::$action' not found.");
		}
	} else {
		die("Class '$controller' not found.");
	}
}