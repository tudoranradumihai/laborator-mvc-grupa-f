<?php
function checkField($field){
	if(array_key_exists("data", $_SESSION)){
		if(array_key_exists($field, $_SESSION["data"])){
			return $_SESSION["data"][$field];
		}
	}
}
function checkRadio($field,$value){
	if(array_key_exists("data", $_SESSION)){
		if(array_key_exists($field, $_SESSION["data"])){
			if($_SESSION["data"][$field]==$value){
				return "checked";
			}
		}
	}
}
function checkCheckbox($field,$value){
	if(array_key_exists("data", $_SESSION)){
		if(array_key_exists($field, $_SESSION["data"])){
			if(in_array($value,$_SESSION["data"][$field])){
				return "checked";
			}
		}
	}
}
?>
<h1>Contact</h1>
<?php
	if(array_key_exists("messages", $_SESSION)){
		foreach($_SESSION["messages"] as $message){
			echo '<div class="alert alert-danger" role="alert">'.$message.'</div>';
		}
		unset($_SESSION["messages"]);
	}
	var_dump($_SESSION["data"]);
?>
<form method="POST" action="index.php?page=Confirmation">
	<div class="form-group">
		<label for="firstname">Firstname</label>
		<input type="text" class="form-control" name="firstname" id="firstname" placeholder="John" value="<?php echo checkField("firstname") ?>">
	</div>
	<div class="form-group">
		<label for="lastname">Lastname</label>
		<input type="text" class="form-control" name="lastname" id="lastname" placeholder="Doe" value="<?php echo checkField("lastname") ?>">
	</div>
	<div class="form-check">
		<input class="form-check-input" type="radio" name="gender" id="gender1" value="1" <?php echo checkRadio("gender",1) ?>>
		<label class="form-check-label" for="gender1">
			Male
		</label>
	</div>
	<div class="form-check">
		<input class="form-check-input" type="radio" name="gender" id="gender2" value="2" <?php echo checkRadio("gender",2) ?>>
		<label class="form-check-label" for="gender2">
			Female
		</label>
	</div>
	<div class="form-group">
		<label for="email">Email Address</label>
		<input type="email" class="form-control" name="email" id="email" placeholder="john.doe@domain.com" value="<?php echo checkField("email") ?>">
	</div>
	<div class="form-group">
		<label for="phone">Phone Number</label>
		<input type="text" class="form-control" name="phone" id="phone" placeholder="0000000000" value="<?php echo checkField("phone") ?>">
	</div>
	<div class="form-group">
		<label for="message">Message</label>
		<textarea class="form-control" name="message" id="message" rows="3"><?php echo checkField("message") ?></textarea>
	</div>
	<div class="form-check form-check-inline">
		<input class="form-check-input" type="checkbox" name="type[]" id="type1" value="1" <?php echo checkCheckbox("type",1) ?>>
		<label class="form-check-label" for="type1">Type 1</label>
	</div>
	<div class="form-check form-check-inline">
		<input class="form-check-input" type="checkbox" name="type[]" id="type2" value="2" <?php echo checkCheckbox("type",2) ?>>
		<label class="form-check-label" for="type2">Type 2</label>
	</div>
	<button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php
if(array_key_exists("data", $_SESSION)){
	unset($_SESSION["data"]);
}
?>