<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){

	$validation = true;
	$messages 	= [];
	$data 		= [];

	foreach($_POST as $key => $value){
		if(empty($value)){
			$validation = false;
			array_push($messages,"Field '$key' is required.");
		} else {
			$data[$key] = $value;
		}
	} 

	if(array_key_exists("email", $data)){
		if(!filter_var($data["email"],FILTER_VALIDATE_EMAIL)){
			$validation = false;
			array_push($messages,"Email format is invalid.");
			unset($data["email"]);
		}
	}

	if(array_key_exists("phone", $data)){
		if(!is_numeric($data["phone"])){
			$validation = false;
			array_push($messages,"Phone format is invalid.");
			unset($data["phone"]);
		}
	}

	if(array_key_exists("phone", $data)){
		if(strlen($data["phone"])!=10){
			$validation = false;
			array_push($messages,"Phone must have 10 characters.");
			unset($data["phone"]);
		}
	}

	if($validation){

	} else {
		$_SESSION["messages"] 	= $messages;
		$_SESSION["data"]		= $data;
		header("Location: index.php?page=contact");
	}

}
