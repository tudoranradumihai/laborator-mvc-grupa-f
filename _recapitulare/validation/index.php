<?php
session_start();
?>
<!DOCTYPE html>
<html>
	<head>
		<?php require "Partials/HeaderResources.php"; ?>
	</head>
	<body>
		<div class="container">
			<header>
				<?php require "Partials/Header.php" ?>
			</header>
			<main>
				<?php
				if(array_key_exists("page",$_GET)){
					$filepath = "Pages/$_GET[page].php";
					if(file_exists($filepath)){
						require $filepath;
					} else {
						echo "<h1>404</h1>";
					}
				} else {
					require "Pages/Home.php";
				}
				?>
			</main>
			<footer>
				<?php require "Partials/Footer.php" ?>
			</footer>
		</div>
		<?php require "Partials/FooterResources.php"; ?>
	</body>
</html>