<?php
return array(
	"GENERAL" => array(
		"DOMAIN" => "http://myshop.com/",
		"REWRITE" => true
	),
	"DATABASE" => array(
		"HOSTNAME" => "localhost",
		"USERNAME" => "root",
		"PASSWORD" => "",
		"DATABASE" => "laborator-mvc-grupa-f"
	)
);