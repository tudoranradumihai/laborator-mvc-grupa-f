<?php

Class ProductsRepository extends Repository {

	public function findByFilter($filter){
		$query = "SELECT * FROM $this->table WHERE name LIKE '%".strtolower($filter)."%' OR description LIKE '%".strtolower($filter)."%'";
		$result = $this->database->query($query);
		$temporary = array();
		while($item = $result->fetch_assoc()){
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($item[$property]);
			}
			array_push($temporary,$object);
		}
		return $temporary;
	}

	public function findByFilters($filters){
		$query = "SELECT * FROM $this->table";

		$temporary = array();
		foreach($filters as $column => $filter){
			if(!empty($filter)){
				if($column=="price"){
					$priceRange = explode("-",$filter);
					array_push($temporary, "(price > $priceRange[0] AND price < $priceRange[1])");
				} else {
					array_push($temporary, "$column='$filter'");
				}
			}
		}
		if(count($temporary)>0){
			$query .= " WHERE ".implode(" AND ",$temporary);
		}
		$result = $this->database->query($query);
		$temporary = array();
		while($item = $result->fetch_assoc()){
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($item[$property]);
			}
			array_push($temporary,$object);
		}
		return $temporary;
	}

	public function findByCategory($category){
		$query = "SELECT * FROM $this->table WHERE category=$category";
		$result = $this->database->query($query);
		$temporary = array();
		while($item = $result->fetch_assoc()){
			$object = new $this->model();
			foreach($object as $property => $value){
				$object->$property = $item[$property];
			}
			array_push($temporary,$object);
		}
		return $temporary;
	}

}