<?php

Class RoutesRepository extends Repository {

	public function findByParameters($controller,$action,$identifier=NULL){
		$query = "SELECT * FROM $this->table WHERE controller LIKE '$controller' AND action LIKE '$action'";
		if($identifier){
			$query .= " AND identifier=$identifier";
		}
		$result = $this->database->query($query);
		$temporary = array();
		while($item = $result->fetch_assoc()){
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($item[$property]);
			}
			array_push($temporary,$object);
		}
		return $temporary;
	}

}