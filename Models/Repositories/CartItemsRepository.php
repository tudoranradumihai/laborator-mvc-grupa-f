<?php

Class CartItemsRepository extends Repository {
	
	public function findByCartAndProduct($cart,$product){
		$query = "SELECT * FROM $this->table WHERE cart=$cart AND product=$product;";
		$result = $this->database->query($query);
		if($result->num_rows!=0){
			$item = $result->fetch_assoc();
			$cartItem = new CartItems();
			foreach($cartItem->getProperties() as $property){
				$cartItem->{"set".ucfirst($property)}($item[$property]);
			}
			return $cartItem;
		} else {
			return NULL;
		}
	}

	public function findByCart($cart){
		$query = "SELECT * FROM $this->table WHERE cart=$cart;";
		$result = $this->database->query($query);
		$temporary = array();
		while($item = $result->fetch_assoc()){
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($item[$property]);
			}
			array_push($temporary,$object);
		}
		return $temporary;
	}

}