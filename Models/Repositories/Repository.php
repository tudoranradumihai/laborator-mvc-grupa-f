<?php

Abstract Class Repository {

	protected $database;
	protected $model;
	protected $table;

	public function __construct(){
		$this->database = new Database();
		$this->model = preg_replace("/Repository$/","",get_called_class());
		$this->table = strtolower(implode("_",preg_split("/(?=[A-Z])/",lcfirst($this->model))));
	}
	
	public function findAll(){
		$query = "SELECT * FROM $this->table;";
		$result = $this->database->query($query);
		$temporary = array();
		while($item = $result->fetch_assoc()){
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($item[$property]);
			}
			array_push($temporary,$object);
		}
		return $temporary;
	}

	public function findByID($id){
		$query = "SELECT * FROM $this->table WHERE id=$id;";
		$result = $this->database->query($query);
		if($result->num_rows==1){
			$item = $result->fetch_assoc();
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($item[$property]);
			}
			return $object;
		} else {
			return NULL;
		}
	}

	public function findByFilter($name,$value,$isNumeric = FALSE){
		if($isNumeric){
			$query = "SELECT * FROM $this->table WHERE $name=$value;";
		} else {
			$query = "SELECT * FROM $this->table WHERE $name LIKE '$value';";
		}
		$result = $this->database->query($query);
		$temporary = array();
		while($item = $result->fetch_assoc()){
			$object = new $this->model();
			foreach($object->getProperties() as $property){
				$object->{"set".ucfirst($property)}($item[$property]);
			}
			array_push($temporary,$object);
		}
		return $temporary;
	}

	public function insert($object){
		$properties = array();
		$values = array();
		foreach($object->getProperties() as $property){
			if(!empty($object->{"get".ucfirst($property)}())){
				array_push($properties,$property);
				array_push($values, '"'.$object->{"get".ucfirst($property)}().'"');
			}
		}
		$query = "INSERT INTO $this->table (".implode(",",$properties).") VALUES (".implode(",",$values).");";
		$result = $this->database->query($query);
		if($result){
			return $this->database->last();
		} else {
			return false;
		}
	}

	public function update($object){
		$values = array();
		foreach($object->getProperties() as $property){
			if(!in_array($property,array("id","createddate","updateddate"))){
				array_push($values,$property."='".$object->{"get".ucfirst($property)}()."'");
			}
		}
		$query = "UPDATE $this->table SET ".implode(",",$values)." WHERE id=".$object->getId();
		return $this->database->query($query);
	}

	public function delete($id){
		$query = "DELETE FROM $this->table WHERE id=$id;";
		return $this->database->query($query);
	}



}