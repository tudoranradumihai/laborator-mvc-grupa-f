<?php

Class Routes extends Model {

	private $id;
	private $url;
	private $controller;
	private $action;
	private $identifier;
	private $additional;
	private $createddate;
	private $updateddate;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getUrl(){
		return $this->url;
	}

	public function setUrl($url){
		$this->url = $url;
	}

	public function getController(){
		return $this->controller;
	}

	public function setController($controller){
		$this->controller = $controller;
	}

	public function getAction(){
		return $this->action;
	}

	public function setAction($action){
		$this->action = $action;
	}

	public function getIdentifier(){
		return $this->identifier;
	}

	public function setIdentifier($identifier){
		$this->identifier = $identifier;
	}

	public function getAdditional(){
		return $this->additional;
	}

	public function setAdditional($additional){
		$this->additional = $additional;
	}

	public function getCreateddate(){
		return $this->createddate;
	}

	public function setCreateddate($createddate){
		$this->createddate = $createddate;
	}

	public function getUpdateddate(){
		return $this->updateddate;
	}

	public function setUpdateddate($updateddate){
		$this->updateddate = $updateddate;
	}

}