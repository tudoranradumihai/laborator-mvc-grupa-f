<?php

Abstract Class Model {

	public function getProperties(){
		$reflection = new ReflectionClass($this);
		$properties = $reflection->getProperties();
		$temporary  = array();
		foreach($properties as $property){
			array_push($temporary, $property->name);
		}
		return $temporary;
	}

}