<?php

Class Cart extends Model {

	private $id;
	private $session;
	private $user;
	private $createddate;
	private $updateddate;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getSession(){
		return $this->session;
	}

	public function setSession($session){
		$this->session = $session;
	}

	public function getUser(){
		return $this->user;
	}

	public function setUser($user){
		$this->user = $user;
	}


	public function getCreateddate(){
		return $this->createddate;
	}

	public function setCreateddate($createddate){
		$this->createddate = $createddate;
	}

	public function getUpdateddate(){
		return $this->updateddate;
	}

	public function setUpdateddate($updateddate){
		$this->updateddate = $updateddate;
	}

}