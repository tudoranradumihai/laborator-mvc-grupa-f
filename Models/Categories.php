<?php

Class Categories extends Model {
	
	private $id;
	private $name;
	private $description;
	private $createddate;
	private $updateddate;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDescription($description){
		$this->description = $description;
	}

	public function getCreateddate(){
		return $this->createddate;
	}

	public function setCreateddate($createddate){
		$this->createddate = $createddate;
	}

	public function getUpdateddate(){
		return $this->updateddate;
	}

	public function setUpdateddate($updateddate){
		$this->updateddate = $updateddate;
	}

}