<?php

Class CartItems extends Model {

	private $id;
	private $cart;
	private $product;
	private $quantity;
	private $createddate;
	private $updateddate;

	public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

	public function getCart(){
		return $this->cart;
	}

	public function setCart($cart){
		$this->cart = $cart;
	}

	public function getProduct($lazyLoad=false){
		if($lazyLoad){
			$productsRepository = new ProductsRepository();
			return $productsRepository->findById($this->product);
		} else {
			return $this->product;
		}
	}

	public function setProduct($product){
		$this->product = $product;
	}

	public function getQuantity(){
		return $this->quantity;
	}

	public function setQuantity($quantity){
		$this->quantity = $quantity;
	}

	public function getCreateddate(){
		return $this->createddate;
	}

	public function setCreateddate($createddate){
		$this->createddate = $createddate;
	}

	public function getUpdateddate(){
		return $this->updateddate;
	}

	public function setUpdateddate($updateddate){
		$this->updateddate = $updateddate;
	}

}