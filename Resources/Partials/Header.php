<div class="row">
	<div class="col-md-6">
		LOGO
	</div>
	<div class="col-md-6 text-right">
		<?php
		if(array_key_exists("user", $_COOKIE)){
			$user = unserialize($_SESSION["user"]);
			echo "Buna ".$user->getFirstname()." ".$user->getLastname()." | ";
			echo '<a href="'.URL::generate("Users","profile").'">My Profile</a> | ';
			if(array_key_exists("admin", $_COOKIE)){
				echo '<a href="'.URL::generate("Users","panel").'">Admin Panel</a> | ';
			}
			echo '<a href="'.URL::generate("Users","disconnect").'">Logout</a>';
		} else {
			echo '<a href="'.URL::generate("Users","login").'">Login</a> |';
			echo '<a href="'.URL::generate("Users","new").'">Register</a>';
		}
			echo ' | <a href="'.URL::generate("Cart","default").'">Cart</a>';

		?>
	</div>
	<div class="col-md-12">
		<ul class="nav justify-content-center">
		  <li class="nav-item">
		    <a class="nav-link active" href="<?php URL::show("Default","default") ?>">Home</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" href="<?php URL::show("Products","search") ?>">Products</a>
		  </li>
		</ul>
	</div>
</div>