<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script>
$(document).ready(function(){
	$("#name").keyup(function(){
		var text = $(this).val().toLowerCase().replace(/ /g,"-");
		$("#routes").val(text);
	});
	$(".quantity").change(function(){
		var id = $(this).attr("data-id");
		$.ajax({
			url: "no-template.php?C=CartItems&A=update",
			type: "POST",
			data: {
				id: id,
				quantity: $(this).val()
			},
			success: function(response){
				$("#price-" + id).html(parseFloat(response).toFixed(2));
				$("#price-hidden-" + id).val(response);
				var total = 0;
				$(".hidden-price").each(function(){
					total = parseInt(total) + parseInt($(this).val());
				});
				$("#total-price").html(parseFloat(total).toFixed(2));
			}
		});
	});

	$(".delete-item").click(function(){
		var id = $(this).attr("data-id");
		$.ajax({
			url: "no-template.php?C=CartItems&A=update",
			type: "POST",
			data: {
				id: id,
				quantity: '0'
			},
			success: function(response){
				$(".item-".id).remove();
				var total = 0;
				$(".hidden-price").each(function(){
					total = parseInt(total) + parseInt($(this).val());
				});
				$("#total-price").html(parseFloat(total).toFixed(2));
			}
		});
	});
});
</script>