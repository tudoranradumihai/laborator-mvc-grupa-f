CREATE TABLE products (
	id INT(11) PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	description TEXT,
	stock INT(11) DEFAULT 0,
	price FLOAT(9,2) NOT NULL,
	promotional FLOAT(9,2) DEFAULT 0,
	category INT(11),
	createddate DATETIME DEFAULT CURRENT_TIMESTAMP,
	updateddate DATETIME ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY (category) REFERENCES categories(id)
);
